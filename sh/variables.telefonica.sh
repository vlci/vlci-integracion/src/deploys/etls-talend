#!/bin/bash

# ETL Box Variables
DEPLOY_FILE_PATH=/opt/etls/sc_vlci/deploys/etls-talend/files
LOGS_PATH=/var/log/etls/sc_vlci/etl

# GITLAB Variables
GITLAB_URL=https://gitlab.com/api/v4/projects
GITLAB_REPO_PREFIX=git@gitlab.com:vlci/vlci-integracion/src/talend/etls/
GITLAB_REPO_SUFIX=.git