#!/bin/bash
set -o pipefail
set -e
RESULT=0
trap catch_errors ERR

function catch_errors() {
    RESULT=$?
}

# Global variables
DEPLOY_SH_PATH=/opt/etls/sc_vlci/deploys/etls-talend/sh

function validateArguments() {
    # Environment
    case $1 in
    PRE)
        ;;
    PRO)
        ;;
    *)
        echo "Uso: $ deploy_etl.sh (PRO|PRE) {ETL_NAME} {ETL_VERSION} {JOB_NAME} {SSH_GITLAB_REPO} {TAGNAME} {COMMENT}" 1>&2
        echo "Example: $ deploy_etl.sh PRO etl_api_loader 5.1 etl_api_loader_wifi ETL_API_LOADER 2023_S09 Sprint_2023_S09" 1>&2
        exit 2
        ;;
    esac
}

function deploy() {
    # Environment
    case $1 in
    PRE)
        ${DEPLOY_SH_PATH}/upload_etl.sh $1 $2 $3 $4
        ;;
    PRO)
        ${DEPLOY_SH_PATH}/transfer_etl.sh $2 $3 $4
        ;;
    *)
        echo "Uso: $ deploy_etl.sh (PRO|PRE) {ETL_NAME} {ETL_VERSION} {JOB_NAME} {SSH_GITLAB_REPO} {TAGNAME} {COMMENT}" 1>&2
        exit 2
        ;;
    esac
    
    LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
    if [ $RESULT -ne 0 ]; then
        echo "ERROR - ${LOG_TIMESTAMP} - ERROR FOUND DURING THE PROCESS"
    else
        echo "INFO - ${LOG_TIMESTAMP} - Now installing the etl..."
        ${DEPLOY_SH_PATH}/download_etl.sh $1 $2 $3 $4 $5 $6 $7
    fi
}

############################################################
# MAIN PROGRAM
############################################################
if [ "$#" -ne 7 ]; then
    echo "Uso: $ deploy_etl.sh (PRO|PRE) {ETL_NAME} {ETL_VERSION} {JOB_NAME} {SSH_GITLAB_REPO} {TAGNAME} {COMMENT}"
    echo "Example: $ deploy_etl.sh PRO etl_api_loader 5.1 etl_api_loader_wifi ETL_API_LOADER 2023_S09 Sprint_2023_S09"
    exit 2
fi

validateArguments $1
deploy $1 $2 $3 $4 $5 $6 $7

LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
if [ $RESULT -ne 0 ]; then
    echo "ERROR - ${LOG_TIMESTAMP} - ERROR FOUND DURING THE PROCESS"
else
    echo "INFO - ${LOG_TIMESTAMP} - PROCESS COMPLETED SUCCESSFULLY"
fi

exit ${RESULT}
