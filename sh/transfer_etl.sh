#!/bin/bash
set -o pipefail
set -e
RESULT=0
trap catch_errors ERR

function catch_errors() {
    RESULT=$?
}

# Global variables
DEPLOY_SH_PATH=/opt/etls/sc_vlci/deploys/etls-talend/sh
CONFIG_PATH=/opt/etls/sc_vlci/deploys

function setArguments() {
    ETL_NAME=$1
    ETL_VERSION=$2
    JOB_NAME=$3
}

function setVariables() {
    # Load Variables
    source ${DEPLOY_SH_PATH}/variables.sh
    source ${CONFIG_PATH}/secrets.sh

    # Global Variables
    TMP_PATH=/opt/etls/sc_vlci/PRE/tmp/deploys/${ETL_VERSION}_${JOB_NAME}

    # Script Variables
    TIMESTAMP=$(date '+%Y%m%d%-H%M')
    DAY=$(date '+%Y%m%d')

    LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
    echo "INFO - ${LOG_TIMESTAMP} - function setVariables: Variables loaded in the bash shell"
}


function download() {
    LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
    echo "INFO - ${LOG_TIMESTAMP} - Check if the job file exists in the gitlab registry"
    wget --header "DEPLOY-TOKEN: ${PRE_GITLAB_DEPLOY_TOKEN}" "${GITLAB_URL}/${PRE_GITLAB_REPO_ID}/packages/generic/${ETL_NAME}/${ETL_VERSION}/${JOB_NAME}.zip" -P ${TMP_PATH}
        
    if [ -f ${TMP_PATH}/${JOB_NAME}.zip ]; then
        LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
        echo "INFO - ${LOG_TIMESTAMP} - Job downloaded"
    else
        LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
        echo "ERROR - ${LOG_TIMESTAMP} - WGET failed"; 
        RESULT=4;
    fi
}

function upload() {
    if [ -f ${TMP_PATH}/${JOB_NAME}.zip ]; then
        CURL_OUT=$(curl --header "DEPLOY-TOKEN: ${PRO_GITLAB_DEPLOY_TOKEN}" --upload-file ${TMP_PATH}/${JOB_NAME}.zip "${GITLAB_URL}/${PRO_GITLAB_REPO_ID}/packages/generic/${ETL_NAME}/${ETL_VERSION}/${JOB_NAME}.zip")
        if echo ${CURL_OUT} | grep -q 401; then
            LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
            echo "ERROR - ${LOG_TIMESTAMP} - CURL unauthorized"; 
            RESULT=4;
        else
            LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
            echo "INFO - Curl ouput: ${CURL_OUT}"
            echo "INFO - ${LOG_TIMESTAMP} - File uploaded"
        fi        
    else 
        LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
        echo "ERROR - ${LOG_TIMESTAMP} - ${TMP_PATH}/${ETL_NAME}.zip does not exist."; 
        RESULT=3;
    fi
}

function cleanup() {
    rm -r /opt/etls/sc_vlci/PRE/tmp/deploys/${ETL_VERSION}_${JOB_NAME}
}

############################################################
# MAIN PROGRAM
############################################################
if [ "$#" -ne 3 ]; then
    echo "Uso: $ transfer_etl.sh {ETL_NAME} {ETL_VERSION} {JOB_NAME}"
    echo "Example: $ transfer_etl.sh etl_api_loader 5.1 etl_api_loader_wifi"
    exit 2
fi

setArguments $1 $2 $3

setVariables

download
if [ $RESULT -eq 0 ]; then
    upload
    cleanup
fi

LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
if [ $RESULT -ne 0 ]; then
    echo "ERROR - ${LOG_TIMESTAMP} - ERROR FOUND DURING THE PROCESS"
else
    echo "INFO - ${LOG_TIMESTAMP} - PROCESS COMPLETED SUCCESSFULLY"
fi

exit ${RESULT}