#!/bin/bash
set -o pipefail
set -e
RESULT=0
trap catch_errors ERR

function catch_errors() {
    RESULT=$?
}

# Global variables
DEPLOY_SH_PATH=/opt/etls/sc_vlci/deploys/etls-talend/sh
CONFIG_PATH=/opt/etls/sc_vlci/deploys

function setArguments() {
    # Environment
    case $1 in
    PRE)
        GITLAB_REPO_ID=${PRE_GITLAB_REPO_ID}
        GITLAB_DEPLOY_TOKEN=${PRE_GITLAB_DEPLOY_TOKEN}
        ;;
    PRO)
        GITLAB_REPO_ID=${PRO_GITLAB_REPO_ID}
        GITLAB_DEPLOY_TOKEN=${PRO_GITLAB_DEPLOY_TOKEN}
        ;;
    *)
        echo "Uso: $ download_etl.sh (PRO|PRE) {ETL_NAME} {ETL_VERSION} {JOB_NAME} {SSH_GITLAB_REPO} {TAGNAME} {COMMENT}" 1>&2
        echo "Example: $ download_etl.sh PRO etl_api_loader 5.1 etl_api_loader_wifi ETL_API_LOADER 2023_S09 Sprint_2023_S09" 1>&2
        exit 2
        ;;
    esac
    ENVIRONMENT=$1
    ETL_NAME=$2
    ETL_VERSION=$3
    JOB_NAME=$4
    GITLAB_REPO_NAME=$5
    GITLAB_REPO_FULL_NAME=${GITLAB_REPO_PREFIX}${GITLAB_REPO_NAME}${GITLAB_REPO_SUFIX}
    TAGNAME=$6
    COMMENT=$7
}

function setVariables() {
    # Load Variables
    source ${DEPLOY_SH_PATH}/variables.sh
    source ${CONFIG_PATH}/secrets.sh

    # Script Variables
    TIMESTAMP=$(date '+%Y%m%d%-H%M')
    DAY=$(date '+%Y%m%d')

    LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
    echo "INFO - ${LOG_TIMESTAMP} - function setVariables: Variables loaded in the bash shell"
}


function download() {

    DEPLOY_PATH=/opt/etls/sc_vlci/${ENVIRONMENT}/${ETL_NAME}/Jobs/${JOB_NAME}

    if [ -d ${DEPLOY_PATH} ]; then
        LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
        echo "INFO - ${LOG_TIMESTAMP} - Check if the job file exists in the gitlab registry"
        wget --header "DEPLOY-TOKEN: ${GITLAB_DEPLOY_TOKEN}" "${GITLAB_URL}/${GITLAB_REPO_ID}/packages/generic/${ETL_NAME}/${ETL_VERSION}/${JOB_NAME}.zip" -P ${DEPLOY_PATH}
        
        if [ -f ${DEPLOY_PATH}/${JOB_NAME}.zip ]; then
            LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
            echo "INFO - ${LOG_TIMESTAMP} - Removing previous installation..."
            echo "DEBUG - ${LOG_TIMESTAMP} - Deploy path = ${DEPLOY_PATH}"
            rm -r ${DEPLOY_PATH}
            mkdir ${DEPLOY_PATH}
            LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
            echo "INFO - ${LOG_TIMESTAMP} - Downloading the job zip file from the gitlab registry..."
            wget --header "DEPLOY-TOKEN: ${GITLAB_DEPLOY_TOKEN}" "${GITLAB_URL}/${GITLAB_REPO_ID}/packages/generic/${ETL_NAME}/${ETL_VERSION}/${JOB_NAME}.zip" -P ${DEPLOY_PATH}
            unzip ${DEPLOY_PATH}/${JOB_NAME}.zip -d ${DEPLOY_PATH}
            LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
            echo "INFO - ${LOG_TIMESTAMP} - File unziped. Removed the wget zip file"
            rm ${DEPLOY_PATH}/${JOB_NAME}.zip
            echo "INFO - ${LOG_TIMESTAMP} - Execution permission to the sh files"
            find ${DEPLOY_PATH} -type f -name "*.sh" -exec chmod u=rwx,go=rx {} \;
        else
            LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
            echo "ERROR - ${LOG_TIMESTAMP} - WGET failed"; 
            RESULT=4;
        fi
    else 
        LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
        echo "ERROR - ${LOG_TIMESTAMP} - ${DEPLOY_PATH} does not exist."; 
        RESULT=3;
    fi
}

function mergeTestMainANDTag() {
    COMMENT=$1

    if [ "$ENVIRONMENT" = "PRO" ]; then
        LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
        
        cd /tmp
        git clone ${GITLAB_REPO_FULL_NAME}
        cd ${GITLAB_REPO_NAME}

        # Merge test to main
        git switch main
        git pull
        echo "INFO - ${LOG_TIMESTAMP} - function mergeTestMainANDTag: Main branch refreshed locally"

        git switch test
        git pull
        echo "INFO - ${LOG_TIMESTAMP} - function mergeTestMainANDTag: Test branch refreshed locally"

        git switch main
        git merge test -m "${COMMENT}" --log --no-edit
        echo "INFO - ${LOG_TIMESTAMP} - function mergeTestMainANDTag: Merged test changes to main"

        git push
        echo "INFO - ${LOG_TIMESTAMP} - function mergeTestMainANDTag: Pushed main branch"
        
        # Tag
        if [ $(git tag -l "${TAGNAME}") ]; then
            LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
            echo "INFO - ${LOG_TIMESTAMP} - function mergeTestMainANDTag: Tag already exists, avoid creating it"
        else
            LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
            git switch main
            echo "INFO - ${LOG_TIMESTAMP} - function mergeTestMainANDTag: Test branch refreshed locally"

            git tag -a ${TAGNAME} -m "${COMMENT}"
            git push origin ${TAGNAME}
            echo "INFO - ${LOG_TIMESTAMP} - function mergeTestMainANDTag: Pushed tag in main branch"
        fi    

        rm -rf ../${GITLAB_REPO_NAME}
    fi
}

############################################################
# MAIN PROGRAM
############################################################
if [ "$#" -ne 7 ]; then
    echo "Uso: $ download_etl.sh (PRO|PRE) {ETL_NAME} {ETL_VERSION} {JOB_NAME} {SSH_GITLAB_REPO} {TAGNAME} {COMMENT}"
    echo "Example: $ download_etl.sh PRO etl_api_loader 5.1 etl_api_loader_wifi ETL_API_LOADER 2023_S09 Sprint_2023_S09"
    exit 2
fi

setVariables

setArguments $1 $2 $3 $4 $5 $6 $7

download

mergeTestMainANDTag $7

LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
if [ $RESULT -ne 0 ]; then
    echo "ERROR - ${LOG_TIMESTAMP} - ERROR FOUND DURING THE PROCESS"
else
    echo "INFO - ${LOG_TIMESTAMP} - PROCESS COMPLETED SUCCESSFULLY"
fi

exit ${RESULT}