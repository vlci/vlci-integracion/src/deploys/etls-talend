#!/bin/bash
set -o pipefail
set -e
RESULT=0
trap catch_errors ERR

function catch_errors() {
    RESULT=$?
}

# Global variables
DEPLOY_SH_PATH=/opt/etls/sc_vlci/deploys/etls-talend/sh
CONFIG_PATH=/opt/etls/sc_vlci/deploys

function setArguments() {
    # Environment
    case $1 in
    PRE)
        GITLAB_REPO_ID=${PRE_GITLAB_REPO_ID}
        GITLAB_DEPLOY_TOKEN=${PRE_GITLAB_DEPLOY_TOKEN}
        ;;
    PRO)
        GITLAB_REPO_ID=${PRO_GITLAB_REPO_ID}
        GITLAB_DEPLOY_TOKEN=${PRO_GITLAB_DEPLOY_TOKEN}
        ;;
    *)
        echo "Uso: $ upload_etl.sh (PRO|PRE) {ETL_NAME} {ETL_VERSION} {JOB_NAME}" 1>&2
        exit 2
        ;;
    esac
    ENVIRONMENT=$1
    ETL_NAME=$2
    ETL_VERSION=$3
    JOB_NAME=$4
}

function setVariables() {
    # Load Variables
    source ${DEPLOY_SH_PATH}/variables.sh
    source ${CONFIG_PATH}/secrets.sh

    # Script Variables
    TIMESTAMP=$(date '+%Y%m%d%-H%M')
    DAY=$(date '+%Y%m%d')

    LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
    echo "INFO - ${LOG_TIMESTAMP} - function setVariables: Variables loaded in the bash shell"
}


function upload() {
    if [ -f ${DEPLOY_FILE_PATH}/${JOB_NAME}.zip ]; then
        CURL_OUT=$(curl --header "DEPLOY-TOKEN: ${GITLAB_DEPLOY_TOKEN}" --upload-file ${DEPLOY_FILE_PATH}/${JOB_NAME}.zip "${GITLAB_URL}/${GITLAB_REPO_ID}/packages/generic/${ETL_NAME}/${ETL_VERSION}/${JOB_NAME}.zip")
        if echo ${CURL_OUT} | grep -q 401; then
            LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
            echo "ERROR - ${LOG_TIMESTAMP} - CURL unauthorized"; 
            RESULT=4;
        else
            LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
            echo "INFO - Curl ouput: ${CURL_OUT}"
            echo "INFO - ${LOG_TIMESTAMP} - File uploaded. Removed it from the local folder"
            rm ${DEPLOY_FILE_PATH}/${JOB_NAME}.zip
        fi        
    else 
        LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
        echo "ERROR - ${LOG_TIMESTAMP} - ${DEPLOY_FILE_PATH}/${ETL_NAME}.zip does not exist."; 
        RESULT=3;
    fi
}

############################################################
# MAIN PROGRAM
############################################################
if [ "$#" -ne 4 ]; then
    echo "Uso: $ upload_etl.sh (PRO|PRE) {ETL_NAME} {ETL_VERSION} {JOB_NAME}"
    echo "Example: $ upload_etl.sh PRO etl_api_loader 5.1 etl_api_loader_wifi"
    exit 2
fi

setVariables

setArguments $1 $2 $3 $4

upload

LOG_TIMESTAMP=$(date '+%Y%m%d%-H%M%S')
if [ $RESULT -ne 0 ]; then
    echo "ERROR - ${LOG_TIMESTAMP} - ERROR FOUND DURING THE PROCESS"
else
    echo "INFO - ${LOG_TIMESTAMP} - PROCESS COMPLETED SUCCESSFULLY"
fi

exit ${RESULT}